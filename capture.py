import cv2
import matplotlib.pyplot as plt
 

class ImageCapture(object):
	def get_image(self, camera):
		retval, im = camera.read()
		return im

	def capture(self,output_file):
		camera_port = 0
		#Number of frames to throw away while the camera adjusts to light levels
		ramp_frames = 2

		# Now we can initialize the camera capture object with the cv2.VideoCapture class.
		# All it needs is the index to a camera port.
		camera = cv2.VideoCapture(camera_port)

		# Ramp the camera - these frames will be discarded and are only used to allow v4l2
		# to adjust light levels, if necessary
		for i in xrange(ramp_frames):
			temp = self.get_image(camera)
		print("Taking image...")
		# Take the actual image we want to keep
		camera_capture = self.get_image(camera)

		#plt.imshow(camera_capture,"aaa")

		# A nice feature of the imwrite method is that it will automatically choose the
		# correct format based on the file extension you provide. Convenient!
		cv2.imwrite(output_file+'.jpg', camera_capture)
		 
		# You'll want to release the camera, otherwise you won't be able to create a new
		# capture object until your script exits
		del(camera)
		return output_file+'.jpg'
