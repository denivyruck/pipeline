#!/usr/bin/env python2

import cv2
import os
import argparse
from image_operations import ImageOperations

#Aquisição, segmentação, representação e classificação

def main():
	io = ImageOperations()

	os.system('rm ./output/* 2>/dev/null')

	input_dir = args.dir[0]

	total = 0
	selected = 0
	for fname in os.listdir(input_dir):
		total += 1
		input_file = input_dir+fname

		#Aquisição
		img = io.read_image(input_file)
		#Pre-processamento
		img = io.gaussian_filter(img)
		#Segmentação
		img = io.otsu_threshold(img)
		#Extração de Feições/Representação
		#Classifica as imagens entre dois valores 
		(x,y), radius = io.extract_circle(img)
		new_image = cv2.imread(input_file)
		cv2.circle(new_image, (int(x), int(y)), int(radius), (0,255,0), 2)
		#Classificação
		if radius > 5:
			selected += 1
			#os.system('cp %s %s' % (input_dir+fname, './output/'))
			io.save_image('./output/'+fname, new_image)

	print('%d/%d birds matched the description' % (selected, total))

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='PIM')
	parser.add_argument('-d', dest='dir', action='store', nargs='+', help='Input file')

	args = parser.parse_args()

	main()
